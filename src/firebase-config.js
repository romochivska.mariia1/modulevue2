import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore/lite'

const firebaseConfig = {
    apiKey: "AIzaSyBBXqP-mVVk78BtX7d80xJ2mWiXVUthTug",
    authDomain: "module2-1848f.firebaseapp.com",
    projectId: "module2-1848f",
    storageBucket: "module2-1848f.appspot.com",
    messagingSenderId: "1058485385418",
    appId: "1:1058485385418:web:b20cf953d53739d8341c33"
  };

const app = initializeApp(firebaseConfig)
const db = getFirestore(app)
export default db
